package com.ventiun.scoringpricingmodel.model;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class FactoringPrePricingDataObject implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@org.kie.api.definition.type.Label(value = "country")
	private java.lang.String country;
	@org.kie.api.definition.type.Label(value = "city")
	private java.lang.String city;
	@org.kie.api.definition.type.Label(value = "campaignId")
	private java.lang.String campaignId;
	@org.kie.api.definition.type.Label(value = "creditScore")
	private java.lang.Double creditScore;
	@org.kie.api.definition.type.Label(value = "daysOfDueDate")
	private java.lang.Integer daysOfDueDate;
	@org.kie.api.definition.type.Label(value = "prePricingResult")
	private java.lang.String prePricingResult;

	public FactoringPrePricingDataObject() {
	}

	public java.lang.String getCountry() {
		return this.country;
	}

	public void setCountry(java.lang.String country) {
		this.country = country;
	}

	public java.lang.String getCity() {
		return this.city;
	}

	public void setCity(java.lang.String city) {
		this.city = city;
	}

	public java.lang.String getCampaignId() {
		return this.campaignId;
	}

	public void setCampaignId(java.lang.String campaignId) {
		this.campaignId = campaignId;
	}

	public java.lang.Double getCreditScore() {
		return this.creditScore;
	}

	public void setCreditScore(java.lang.Double creditScore) {
		this.creditScore = creditScore;
	}

	public java.lang.Integer getDaysOfDueDate() {
		return this.daysOfDueDate;
	}

	public void setDaysOfDueDate(java.lang.Integer daysOfDueDate) {
		this.daysOfDueDate = daysOfDueDate;
	}

	public java.lang.String getPrePricingResult() {
		return this.prePricingResult;
	}

	public void setPrePricingResult(java.lang.String prePricingResult) {
		this.prePricingResult = prePricingResult;
	}

	public FactoringPrePricingDataObject(java.lang.String country,
			java.lang.String city, java.lang.String campaignId,
			java.lang.Double creditScore, java.lang.Integer daysOfDueDate,
			java.lang.String prePricingResult) {
		this.country = country;
		this.city = city;
		this.campaignId = campaignId;
		this.creditScore = creditScore;
		this.daysOfDueDate = daysOfDueDate;
		this.prePricingResult = prePricingResult;
	}

}